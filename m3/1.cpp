#include <vector>
#include <string>
#include <iostream>
#include <math.h>
#include <fstream>

/*
 * ������ 1. ���-������� (6 ������)
���������� ��������� ������ ���� ���������� ����� �� ������ ������������
���-������� � �������� ����������. �������� ������ �������� � ������� �� ��������
��������� ����.
���-������� ������ ������ ���� ����������� � ������� ���������� ��������
���������� ������� �������.
��������� ������ ������� ������ ���� ������ 8-��. ��������������� ����������
��� ���������� ��������� � ������, ����� ����������� ���������� ������� ���������
3/4.
��������� ������ ������ ������������ �������� ���������� ������ � ���������,
�������� ������ �� ��������� � �������� �������������� ������ ������ ���������.
1_2.? ��� ���������� �������� ����������� ������� �����������.
 */

using namespace std;

size_t doubleHash(size_t, size_t, int m, int i);


#define NIL 0
#define FILLED 1
#define DELETED 2

class CHashTableNode {
private:
    string key;
    int nodeState;
public:
    string getKey() { return key; }

    void setKey(const string s) { key = s; }

    void changeState(int state) { nodeState = state; }

    CHashTableNode(const string s) : key(s) { nodeState = FILLED; }

    CHashTableNode() { nodeState = NIL; }

    int getNodeState() { return nodeState; }
};

class CHashTable {
public:
    int Has(string &key);

    int Add(string &key);

    bool Delete(string &key);

    void Resize();

    CHashTable(int size) { table = vector<CHashTableNode>(size); }

    CHashTable() { table = vector<CHashTableNode>(8); };
    size_t getHash(string &str, int a);
    size_t h1(string key);
    size_t h2(string key);
private:
    int count =0;
    vector<CHashTableNode> table;

    void checkLoadFactor();
};

size_t CHashTable::getHash(string &str, int a) {
    size_t hash = 0;
    for (auto c: str) {
        hash = (hash * a + c) % table.size();
    }
    return hash;
}
size_t CHashTable::h1(string key){
    return getHash(key, 9);
}
size_t CHashTable::h2(string key){
    return getHash(key, 11);
}
void CHashTable::checkLoadFactor() {

    float loadFactor = (float) count / table.size();
    //cout<<"loadFactor "<<loadFactor<<endl;
    if (loadFactor >= 0.75) {
        Resize();
    }
}

int CHashTable::Add(string &key) {
    size_t h_1 = h1(key);
    size_t h_2 = h2(key);
    for (int i = 0; i < table.size(); ++i) {
        size_t j = doubleHash(h_1,h_2, table.size(), i); //probe sequence
        //cout << "hash "<< j << endl;
        int state = table[j].getNodeState();
        if (state == NIL || (state == DELETED && table[j].getKey() == key)) {
            table[j].setKey(key);
            table[j].changeState(FILLED);
            count++;
            checkLoadFactor();
            return j;
        } else if (table[j].getKey()== key) {
            return -1;
        }
    }
    //hashtable overflow
    Resize();
    return Add(key);
}



size_t doubleHash(size_t h_1, size_t h_2, int m, int i) {
    return ((h_1) % m + i * (h_2)) % m;
}

void CHashTable::Resize() {
    CHashTable newTable(table.size() * 2);
    //cout << "resizing..." << endl;
    string key;
    for (int i = 0; i < table.size(); ++i) {
        if (table[i].getNodeState() == FILLED) {
            key = table[i].getKey();
            newTable.Add(key);
        }
    }
    *this = newTable; // 2x size of table
}

int CHashTable::Has(string &key) {
    size_t h_1 = h1(key);
    size_t h_2 = h2(key);
    for (int i = 0; i < table.size(); ++i) {

        size_t  j = doubleHash(h_1,h_2,table.size(), i); //probe sequence
        //cout << "hash "<< j << endl;
        int state = table[j].getNodeState();
        if (state == NIL) {
            return -1;
        } else if (state == FILLED && table[j].getKey() == key) {
            return j; //returns index of element (for Delete method)
        }
    }
    return -1;
}

bool CHashTable::Delete(string &key) {
    int index;
    if ((index = Has(key)) == -1) {
        return false;
    }
    table[index].changeState(DELETED);
    count--;
    return true;
}

int main() {
    CHashTable table(8);
    string type, key;
    while(1) {

        cin >> type >> key;
        if(cin.eof())
            break;
        bool result = false;
        if (type == "+") {
            result = -1 != table.Add(key);
        } else if (type == "?") {
            result = -1 != table.Has(key);
        } else if (type == "-") {
            result = table.Delete(key);
        }

        cout << (result ? "OK\n" : "FAIL\n");
    }

    return 0;

}