/**
* ������ 3. ��������� ������ (4 �����)
���� ����� N < 106
� ������������������ ��� ����� ����� �� [-231..231] ������ N.
��������� ��������� ������ �� N �����, ����������������� ������ ����� {Xi, Yi}.
������ ���� ����� {Xi, Yi} ���������� ���� Xi � ��������� Yi � ���������� ������.
���������� ���� � ��������� ������ ���������� ������ ������� ���������,
������������� �� ������:
? ��� ���������� ���� (x, y) ���������� ����� �� ����� �� ���� P � �������
�����������. ����� �������� ��������� ��������� �� ����� x ���, ����� �
������ ��������� ��� ����� ������ x, � �� ������ ������ ��� ����� x.
������������ ��� ������ �������� ��������� ��� ������ ���� (x, y). ����� ����
�������� �� ����� ���� P.
3_1. ?��������� ������� ������ �������� ������ ������ � ����������� ������. �������
����� ���� ������������.
*/

#include <iostream>

struct NNode {
    int key;
    NNode *left;
    NNode *right;
};
using namespace std;

struct DNode {
    int key;
    DNode *left;
    DNode *right;
    int priority;
};

class Treap {
public:
    DNode *empty_DNode();

    DNode *create_DNode(int key, int priority);

    void insert(int key, int priority);

    void insert(DNode *&t, DNode *it);
    static void split(DNode *_root, int key, DNode *&left, DNode *&right);
    Treap();

    ~Treap();

    int getDepth(DNode *);

    int getDepth();

private:
    void destroy_tree(DNode *);

    DNode *root;
};

int Treap::getDepth() {
    return getDepth(root);
}

int Treap::getDepth(DNode *leaf) {
    if (leaf == NULL) {
        return 0;
    }
    int resL = getDepth(leaf->left);
    int resR = getDepth(leaf->right);
    return (resL > resR) ? resL + 1 : resR + 1;
}

Treap::Treap() {
    root = NULL;
}

Treap::~Treap() {
    destroy_tree(root);
}

void Treap::destroy_tree(DNode *leaf) {
    if (leaf != NULL) {
        destroy_tree(leaf->left);
        destroy_tree(leaf->right);
        delete leaf;
    }
}

DNode *Treap::empty_DNode() {
    DNode *newDNode = new DNode;
    newDNode->left = NULL;
    newDNode->right = NULL;
    return newDNode;
}

DNode *Treap::create_DNode(int key, int priority) {
    DNode *newDNode = empty_DNode();
    newDNode->key = key;
    newDNode->priority = priority;
    return newDNode;

}

void Treap::split(DNode *_root, int key, DNode *&left, DNode *&right) {
    if (!_root) {
        left = right = NULL;
    }
    else if (key < _root->key) {
        split(_root->left, key, left, _root->left);
        right = _root;
    }
    else {
        split(_root->right, key, _root->right, right);
        left = _root;
    }
}

void Treap::insert(DNode *&_root, DNode *newNode) {
    if (!_root) {
        _root = newNode;
    }
    else if (newNode->priority > _root->priority) {
        split(_root, newNode->key, newNode->left, newNode->right);
        _root = newNode;
    }
    else {
        if (newNode->key < _root->key){
            insert( _root->left, newNode);
        } else{
            insert( _root->right, newNode);
        }
    }
}

void Treap::insert(int key, int priority) {
    DNode *newNode = create_DNode(key, priority);
    insert(root, newNode);
}
//////
class NaiveSearchTree {
public:
    NaiveSearchTree();

    ~NaiveSearchTree() { destroy_tree(root); };

    void insert(int key);

    int getDepth(NNode *);

    int getDepth();

private:
    void destroy_tree(NNode *leaf);

    void insert(int key, NNode *leaf);

    NNode *root;
};

NaiveSearchTree::NaiveSearchTree() {
    root = NULL;
}

void NaiveSearchTree::destroy_tree(NNode *leaf) {
    if (leaf != NULL) {
        destroy_tree(leaf->left);
        destroy_tree(leaf->right);
        delete leaf;
    }
}

void NaiveSearchTree::insert(int key, NNode *leaf) {
    if (key < leaf->key) {
        if (leaf->left != NULL)
            insert(key, leaf->left);
        else {
            leaf->left = new NNode;
            leaf->left->key = key;
            leaf->left->left = NULL;
            leaf->left->right = NULL;
        }
    } else if (key >= leaf->key) {
        if (leaf->right != NULL)
            insert(key, leaf->right);
        else {
            leaf->right = new NNode;
            leaf->right->key = key;
            leaf->right->left = NULL;
            leaf->right->right = NULL;
        }
    }
}

int NaiveSearchTree::getDepth() {
    return getDepth(root);
}

int NaiveSearchTree::getDepth(NNode *leaf) {
    if (leaf == NULL) {
        return 0;
    }
    int resL = getDepth(leaf->left);
    int resR = getDepth(leaf->right);
    return (resL > resR) ? resL + 1 : resR + 1;
};

void NaiveSearchTree::insert(int key) {
    if (root != NULL)
        insert(key, root);
    else {
        root = new NNode;
        root->key = key;
        root->left = NULL;
        root->right = NULL;
    }
}

int main() {
    int N;
    cin >> N;
    Treap decartesTree = Treap();
    NaiveSearchTree naiveSearchTree = NaiveSearchTree();
    int value, priority;
    for (int i = 0; i < N; ++i) {
        cin >> value >> priority;
        decartesTree.insert(value, priority);
        naiveSearchTree.insert(value);
    }
    cout << naiveSearchTree.getDepth() - decartesTree.getDepth() << endl;
}