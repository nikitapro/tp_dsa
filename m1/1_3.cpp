/**
 * ?���� ��� ������������ �����: a/b � c/d. ������� �� � ��������� ����������� � ���� ������������
 * ����� m/n. ������� ����� m � n.
 * a,b,c,d <=1000
 */



#include <iostream>

#define MAX_VALUE 1000

void calc(int a, int b, int c, int d);

using namespace std;

int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    if (a > MAX_VALUE || b > MAX_VALUE || c > MAX_VALUE || d > MAX_VALUE ||
        a == 0 || b == 0 || c == 0 || d == 0) {
        return -1;
    }
    calc(a, b, c, d);
    return 0;
}

void calc(int a, int b, int c, int d) {
    int denom = b * d;
    int del = a * d + b * c;
    for (int i = 2; i < denom;) {
        //���� � ��������� � ����������� ������� ������ �� �����
        if (del % i == 0 && denom % i == 0) {
            del /= i;
            denom /= i;
        }
        else{
            ++i;
        }
    }
    cout << del << " " << denom << endl;
}