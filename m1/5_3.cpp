/**
 * 5_3. ��������������.
 *���� ������������������ N ��������������� ��������� ������ � ������ (wi,hi). ��������������
 * �����������, ������� � ����� (0, 0), �� ��� �� �������� ���� �� ������ (������). ��������� ����� M �
 * ������� ������������� �������������� (������������� ���� ���������), ������� ����� �������� �� ����
 * ������.
*/

#define MAX_VALUE 10000
#define MIN_VALUE 1

#include <iostream>

using namespace std;

struct Rect {
    int width;
    int height;
};

class Stack {
public:
    Stack(int size);

    ~Stack() { delete[] buffer; };

    void push(Rect);

    Rect pop();

    void enlarge();

    int cut(int height);

private:
    int size;
    int maxSize;
    Rect *buffer;
};

void Stack::enlarge() {
    int newSize = maxSize * 2;
    Rect *newBuf = new Rect[newSize];
    for (int i = 0; i < maxSize; i++) {
        newBuf[i] = buffer[i];
    }
    buffer = newBuf;
    maxSize = newSize;

}

Stack::Stack(int size) {
    this->size = 0;
    maxSize = size;
    buffer = new Rect[maxSize];
}

void Stack::push(Rect a) {
    if (size >=maxSize) {
        enlarge();
    }
    buffer[size++] = a;
}

Rect Stack::pop() {
    return buffer[--size];
}

int Stack::cut(int height) {
    int square, maxSquare = 0, width = 0;
    Rect a;
    for (int i = size; i > 0; i--) {
        a = pop();
        if (a.height < height) {
            push(a);
            break;
        }
        width += a.width;
        square = a.height * width;
        if (square >= maxSquare) {
            maxSquare = square;
        }
    }
    a.width = width;
    a.height = height;
    push(a);
    return maxSquare;
}

int main(void) {
    int N;
    Rect rect;
    cin >> N;
    if (N < MIN_VALUE || N > MAX_VALUE) {
        return -1;
    }
    int prevHeight = 0, square, maxSquare = 0;
    Stack stack(10);
    for (int i = 0; i < N; i++) {
        cin >> rect.width >> rect.height;
        if (rect.height < prevHeight) {
            if (maxSquare <= (square = stack.cut(rect.height))) {
                maxSquare = square;
            }
        }
        stack.push(rect);
        prevHeight = rect.height;
    }
    if (maxSquare <= (square = stack.cut(0))) {
        maxSquare = square;
    }
    cout << maxSquare;
    return 0;
}
