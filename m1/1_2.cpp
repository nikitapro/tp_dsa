/**
 * ?������� ���������� ������������ ����� n �� ������� ���������. ������� ��������� ������ ����
 * ����������� �� ����������� � ��������� ���������.
 * 2 <= n <= 10^6
 */

#include <iostream>
#define MAX_NUM 1000000
#define MIN_NUM 2
void calcAndPrintSimpleMult(int n);


using namespace std;

int main() {
    int n;
    cin >> n;
    if (n < MIN_NUM || n > MAX_NUM)
        return -1;
    calcAndPrintSimpleMult(n);
    return 0;
}
void calcAndPrintSimpleMult(int n){
    for (int i = 2; i <= n;) {
        if (n % i == 0) {
            cout << i;
            n /= i;
            if (n > 1)
                cout << " ";
        } else
            i++;
    }

}