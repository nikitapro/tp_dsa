/**
 * 2_4.? ������������. � ���� ��������� N �������, ��������������� ������� �� 1 �� N. ����� ���������

������� k���� �� ��� ���, ���� �� ������� ������ ���� �������. (��������, ���� N=10, k=3, �� �������

����� 3��, ����� 6��, ����� 9��, ����� 2��, ����� 7��, ����� 1��, ����� 8��, �� ��� � 5��, � ����� 10��. �����

�������, ������� 4��.) ���������� ���������� ����� ����������.

N, k ? 10000.
 */
#define MAX_VALUE 10000

#include <iostream>

using namespace std;

/**
 * ������� � �����
 */
class Human {
private:
    int position;
    Human *next;
    Human *prev;
public:
    static Human *getHumanCircle(int size);

    int getPosition() { return position; }

    Human *getNext() { return next; }

    Human *getPrev() { return prev; }

    void setNext(Human *next) { this->next = next; }

    void setPrev(Human *prev) { this->prev = prev; }
};

Human *Human::getHumanCircle(int size) {
    Human *humanCircle = new Human[size];
    for (int i = 1; i < size - 1; ++i) {
        humanCircle[i].position = i;
        humanCircle[i].next = &humanCircle[i + 1];
        humanCircle[i].prev = &humanCircle[i - 1];
    }
    //��������� ������� � ����� ��������� �� �������, � ������ �� ����������
    humanCircle[size - 1].position = size - 1;
    humanCircle[size - 1].next = &humanCircle[0];
    humanCircle[size - 1].prev = &humanCircle[size - 2];
    humanCircle[0].position = 0;
    humanCircle[0].next = &humanCircle[1];
    humanCircle[0].prev = &humanCircle[size - 1];
    return humanCircle;
}

void count(Human *, int);


int main() {
    int n, k;
    cin >> n >> k;
    if (k > n || k < 1 || n < 1 || k > MAX_VALUE || n > MAX_VALUE) {
        return -1;
    }
    Human *circle = Human::getHumanCircle(n);
    count(circle, k);
    return 0;
}

void count(Human *circle, int k) {
    Human *current = circle; //��������� �� �������� �������� ��� ��������
    for (int j = 0; j < k - 1; ++j) {
        current = current->getNext(); //���������� �������� �� k-���� ��������
    }
    while (current->getNext()!= current) { //���� �� �������� ��������
        //������� �������� �� �����, ����� ������ ��� �������
        Human *prev = current->getPrev();
        Human *next = current->getNext();
        prev->setNext(next);
        next->setPrev(prev);
        current = prev;
        for (int i = 0; i < k; ++i) { //������� � ���������� �� �����
            current = current->getNext();
        }
    }
    cout<<current->getPosition()+1<<endl;
    delete circle;

}

