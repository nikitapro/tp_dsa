/**
 * ?������� �������� ����������� ����� �� 1 �� n, ��������� ������ O(n) �������� �������� �
 * ��������� (���������� ������������ ������).
 * n<=1000
 */
double pow2(int num);

void printSquares(int num);
#define MAX_VALUE 1000
#include <iostream>

using namespace std;

int main() {
    int n;
    cin >> n;
    if (n <1 || n >MAX_VALUE)
        return -1;
    printSquares(n);
    return 0;
}

void printSquares(int num) {
    for (int i = 1; i <= num; ++i) {
        cout << pow2(i) << " ";
    }
}
/*
 * ������� ������ ������������ ����� N ����� ����� N ������ �������� �����
 */
double pow2(int num) {
    int a = 1;
    for (int i = 0; i <= num; ++i) {
        a = a + 2 * i - 1;
    }
    return a;
}