/**
 * ?4_3. ����������� ������� � ������� ���� ������. ������������ ����, ������������� � �������
 * ������������� ������.
 */

#define PUSH_BACK 3
#define POP_FRONT 2
#define TRUE 1
#define FALSE 0

#include <iostream>

using namespace std;

class Stack {
private:
    int *buf;
    int size;
    int headIndex;
public :
    Stack();

    ~Stack() { delete[] buf; }

    void push(int value);

    bool isEmpty(){return (headIndex > 0) ? FALSE : TRUE;}

    int pop();
};

Stack::Stack() {
    size = 1;
    buf = new int[size];
    headIndex = 0;
}

void Stack::push(int value) {
    if (headIndex != 0) {
        buf = (int *) realloc(buf, (++size) * sizeof(value));
    }
    buf[headIndex++] = value;
}

int Stack::pop() {
    if (headIndex > 0) {
        return buf[--headIndex];
    } else {
        return -1;
    }
}

class Queue {
private:
    Stack *front;
    Stack *back;
public :
    Queue();

    ~Queue() {
        delete front;
        delete back;
    }

    void push_back(int value);
    int pop_front();
};

Queue::Queue() {
    front = new Stack;
    back = new Stack;
}

void Queue::push_back(int value) {
    back->push(value);
}

int Queue::pop_front() {
    if (front->isEmpty()){
        if (back->isEmpty()){
            return -1;
        }
        while (!back->isEmpty()){
            front->push(back->pop());
        }
    }
    return front->pop();
}


int main() {
    int n;
    cin >> n;
    if (n < 1) {
        return -1;
    }
    int *comm = new int[n]; //�������
    int *val = new int[n]; //��������
    Queue queue;

    bool stringsMatch = TRUE;

    for (int i = 0; i < n; i++) {
        cin >> comm[i] >> val[i];
        if (comm[i] != PUSH_BACK && comm[i] != POP_FRONT) {
            return -1;
        } else {
            if (comm[i] == PUSH_BACK) {
                queue.push_back(val[i]);
            }
            if (comm[i] == POP_FRONT) {
                if (queue.pop_front() != val[i]) {
                    stringsMatch = FALSE;
                    break;
                }
            }
        }
    }
    delete[] comm;
    delete[] val;

    if (stringsMatch) {
        cout << "YES" << endl;
    } else { cout << "NO" << endl; }

    return 0;
}