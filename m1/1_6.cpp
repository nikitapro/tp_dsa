/**
 * ?��� ������ ����� ����� A[0..n). �� ��������� ������ �������� ����������� �������� ������� A �
 * �������� ������� �� O(n).
 */
#include <iostream>

void revertAndPrint(int *arr, int size);

using namespace std;

#define MAX_SIZE 10000

int main() {
    int size;
    cin>>size;
    if (size > MAX_SIZE || size < 1)
        return -1;
    int* arr = new int[size];
    for (int i = 0; i < size; ++i) {
        cin>>arr[i];
    }
    revertAndPrint(arr, size);

    return 0;
}
void revertAndPrint(int *arr, int size){
    for (int i = 0; i < size/2; ++i) {
        int tmp = arr[i];
        arr[i] = arr[size-i-1];
        arr[size-i-1] = tmp;
    }
    for (int j = 0; j < size; ++j) {
        cout<<arr[j]<<" ";
    }
}