/*
 * ?�����, �� ������� ����� ������������ n! = 1 * 2 * 3 * ... * n.
n ? 1000.
 */

int calc(int n);

#include <iostream>

using namespace std;

int main() {
    int n;
    cin >> n;
    if (n >1000){
        return -1;
    }
    cout << calc(n) << endl;
    return 0;
}
/**
 * � ������ � �����������, � ���������� ������ ������ ����� ��� �������
 * ������� ������������ ������ ���-�� ���, ����� ��������� ������� �� 5
 */
int countZeros(int n) {
    int count = 0;
    while (n % 5 == 0) {
        count++;
        n /= 5;
    }
    return count;
}

/**
 * ������������ ���������� ����� �� ����� ����������
 */
int calc(int n) {
    int total_count = 0;
    while (n > 1) {
        total_count += countZeros(n--);
    }
    return total_count;
}