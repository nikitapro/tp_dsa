/**
 * ?���� ����������� ����� N. ����������� N � ���� A + B, ���, ��� ���(A, B) ����������, A ? B. �������
 * A � B. ���� �������� ��������� ������� � ������� ����� � ����������� A.
 * n<=10^7
 */


void calcMaxNod(int n);

int nod(int a, int b);

#define MAX_VALUE 10000000

#include <iostream>

using namespace std;

int main() {
    int n;
    cin >> n;
    if (n > MAX_VALUE || n < 1)
        return -1;
    calcMaxNod(n);
    return 0;
}

void calcMaxNod(int n) {
    int maxA = 1, maxB = n-1;
    for (int i = 2; i <= n / 2; i++) {
        //���� ����������� �������� ��� n
        if (n % i == 0) {
            maxA = n / i; //A - ������������ ��������
            maxB = n - maxA;
            break;
        }
    }
    cout<<maxA<<" "<<maxB<<endl;
}
