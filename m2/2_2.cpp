/**
 * 2_2. ������� ��������.
��� �������� ����� ������������ ������ ���������. �����, ������������� �� ���������� �����
���� ����� ����� �� �����.
����� ������� ��� ���������� ����� ����� 1,2,3 ����� ������������� ������ �����, �
����������� �� ������� ����������.
 * 
 * 
 * 
 */
#include <iostream>

using namespace std;
#define MAX_INPUT_NUMBER 1000000000
#define MAX_SUM 2000000000

class Heap {
    int *elements; 
    int HeapSize; 
public:
    void insert(int); 
    int getMin();  
    void heapify(int);
    Heap(int N);
};

Heap::Heap(int N) {
    elements = new int[N];
    HeapSize = 0;
}

void Heap::insert(int n) {
    int i, parent;
    i = HeapSize;
    elements[i] = n;
    parent = (i - 1) / 2;
    while (parent >= 0 && i > 0) {
        if (elements[i] < elements[parent]) {
            int temp = elements[i];
            elements[i] = elements[parent];
            elements[parent] = temp;
        }
        i = parent;
        parent = (i - 1) / 2;
    }
    HeapSize++;
}

void Heap::heapify(int i) {
    int left, right;
    int temp;
    left = 2 * i + 1;
    right = 2 * i + 2;
    if (left < HeapSize) {
        if (elements[i] > elements[left]) {
            temp = elements[i];
            elements[i] = elements[left];
            elements[left] = temp;
            heapify(left);
        }
    }
    if (right < HeapSize) {
        if (elements[i] > elements[right]) {
            temp = elements[i];
            elements[i] = elements[right];
            elements[right] = temp;
            heapify(right);
        }
    }
}

int Heap::getMin() {
    if (HeapSize == 0) {
        return -1;
    }
    int x;
    x = elements[0];
    elements[0] = elements[HeapSize - 1];
    HeapSize--;
    heapify(0);
    return (x);
}

int calcSum(Heap h) {
    int result = 0;
    int sum = 0;
    //������� ������ 2 ��������
    int a = h.getMin();
    int b = h.getMin();
    while (a != -1 && b != -1) {
        sum = a + b;
        if (sum > MAX_SUM){
            return -1;
        }
        result+=sum;
        //�������� ���� ����� � ����
        h.insert(sum);
        //������� 2 ��������� ����������� ��������
        a = h.getMin();
        b = h.getMin();
    }
    return result;
}

int main() {

    int N;
    cin >> N;
    if (N < 0) {
        return -1;
    }
    Heap heap(N);
    int buf;
    for (int i = 0; i < N; ++i) {
        cin >> buf;
        if (buf > MAX_INPUT_NUMBER) {
            return -1;
        }
        heap.insert(buf);
    }
    int result = calcSum(heap);
    //��������, �� ��������� �� ����. �������� �����
    if (result == -1){
        return -1;
    }
    cout << result << endl;
    return 0;
}