/**
 * ���������� ��������.

���� ������������������ ����� ����� �� ��������� (-10^9 .. 10^9). ����� ������������������ ��

������ 10^6. ����� �������� �� ������ � ������. ���������� ����� �� �������.

����� ���������� ��������� n, � ����� �������� � ������� a = a[i]: i �� [0..n-1].

��������� ���������� ���������� ����� ��� �������� (i,j) �� [0..n-1], ��� (i < j � a[i] > a[j]).
 */

#include <stdint.h>
#include <iostream>
#include <vector>
#include "stdio.h"
#include "stdlib.h"

#define MAX_LENGTH 1000000
#define MAX_VALUE 1000000000

using namespace std;

int64_t merge(int *a, int *temp, size_t left, size_t mid, size_t right) {
    int64_t counter{};

    size_t i = left;
    size_t j = mid;
    size_t k = left;

    while (i < mid && j <= right) {

        if (a[i] <= a[j]) {
            temp[k++] = a[i++];
        } else {
            temp[k++] = a[j++];
            counter += mid - i;
        }
    }

    while (i < mid) {
        temp[k++] = a[i++];
    }

    while (j <= right) {
        temp[k++] = a[j++];
    }

    for (i = left; i <= right; ++i) {
        a[i] = temp[i];
    }

    return counter;
}

int64_t sort(int *a, int *temp, size_t left, size_t right) {
    int64_t counter{};
    size_t mid;

    if (right > left) {
        mid = (right + left) / 2;
        counter = sort(a, temp, left, mid);
        counter += sort(a, temp, mid + 1, right);
        counter += merge(a, temp, left, mid + 1, right);
    }

    return counter;
}

int main() {
    vector<int> values;
    int buf = 0;
    while (!cin.eof()) {
        cin >> buf;
        values.push_back(buf);
    }

    values.resize(values.size() - 1);
    int *temp = new int[values.size() - 1];
    cout << sort(&values[0], &temp[0], 0, values.size() - 1)<<endl;
    return 0;
}