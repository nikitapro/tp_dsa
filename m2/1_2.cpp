/**
 * 1_2. ������� 1.

������ N ����� �� ���������. ������� (N-1)-������� �������������������� ����������� �������,

���������� ����� ��� ��� �����.

��������: ������� ������� � ������� ����������� x-����������. ���� ������� ��� ����� �

���������� x-�����������, �� ����������� ������ �� �����, � ������� y-���������� ������.
 */
#include <iostream>
#include <vector>

using namespace std;

class Point {
private:
    int x;
    int y;
public:
    Point(int x, int y){this->x = x; this->y = y;}
    Point(){};
    int getX(){return x;}
    int getY(){return y;}
    bool operator < (const Point p2);
};
bool Point::operator<(const Point p2) {
    if (p2.x < x){
        return false;
    }
    else if (p2.x > x){
        return true;
    }
    else{
        return p2.y > y;
    }
}
vector<Point> insert_sort(vector<Point> points) {
    int j;
    for ( int i = 1; i < points.size(); ++i ) {
        Point temp = points.at(i);
        for ( j = i; j > 0 && temp < points.at(j-1); --j ) {
            points.at(j) = points.at(j-1);
        }
        points.at(j) = temp;
    }
    return points;
}
vector<Point> getLine(vector<Point> points) {
    points = insert_sort(points);
    return points;
}

int main() {
    int N;
    cin >> N;
    if (N < 1){
        return -1;
    }
    vector<Point> points;
    int x, y;

    for (int i = 0; i < N; ++i) {
        cin >> x >> y;
        points.push_back(*new Point(x,y));
    }
    vector<Point> line = getLine(points);
    Point p;
    for (int j = 0; j < line.size(); ++j) {
        p = line.at(j);
        cout<<p.getX()<<" "<<p.getY()<<endl;
    }
    return 0;
}
