/**
 * 3_3. �������� ������ 1.
�� �������� ������ �������� N ��������. �������� ���������� ������ � ������� ������ �������
������� (Li � Ri). ����� ����� ���������� ����� �������� ������.
 */
#include <iostream>

using namespace std;


void downHeap(int *a, int i, int size) {
    int tmp;
    int childIndex;
    tmp = a[i];
    while (i <= size / 2) { //���� ���� �������� ��������
        childIndex = 2 * i;
        if (childIndex < size && a[childIndex] < a[childIndex + 1])
            childIndex++;
        if (tmp >= a[childIndex]) break;
        a[i] = a[childIndex];
        i = childIndex;
    }
    a[i] = tmp;
}

void heapSort(int *a, int size) {
    int i;
    for (i = size / 2 - 1; i >= 0; i--) {
        downHeap(a, i, size - 1);
    }
    for (i = size - 1; i > 0; i--) {
        swap(a[0], a[i]);
        downHeap(a, 0, i - 1);
    }
}
int calcTotalLength(int *in, int *out, int size) {
    int length = 0;
    int A, B;
    int previousB = in[0];
    for (int i = 0; i < size; i++) { //i ��� ��������� �����, j - ��� ��������
        A = in[i];
        B = out[i];
        length += B - A;
        if (A < previousB) { //���� ����������� � �������������� ����� �� ������ �������������� �����
            length -= previousB - A;
        }
        previousB = B;
    }
    return length;
}

int main() {
    int N;
    cin >> N;
    if (N < 0) {
        return -1;
    }
    int *in = new int[N];
    int *out = new int[N];
    for (int i = 0; i < N; ++i) {
        cin >> in[i];
        cin >> out[i];
    }
    heapSort(in, N);
    heapSort(out, N);
    cout << calcTotalLength(in, out, N) << endl;
}